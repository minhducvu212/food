package com.example.minhd.food;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class ViewpagerActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener, View.OnClickListener {
    private ViewPager vpRe;
    private TabLayout tabLayout;
    private FragmentAdapter mAdapter;
    private LinearLayout linear;
    private DrawerLayout drawer;
    private ImageView img ;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager_fragment);
        initviews();
    }

    private void initviews() {
        vpRe = (ViewPager) findViewById(R.id.vp_Re);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        linear = (LinearLayout) findViewById(R.id.click);
        img = (ImageView) findViewById(R.id.btn_colapse) ;
        linear.setOnClickListener(this);
        mAdapter = new FragmentAdapter(getSupportFragmentManager());
        vpRe.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(vpRe);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#C8996B"));
        tabLayout.setBackgroundResource(R.drawable.shape);
        tabLayout.getTabAt(0).setText("Recpis" + " ");
        tabLayout.getTabAt(1).setText("Wine" + " ");
        tabLayout.getTabAt(2).setText("Profile" + " ");

        vpRe.addOnPageChangeListener(this);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(ViewpagerActivity.this, MainActivity.class);
        startActivity(intent);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.slide_to_left);
        img.startAnimation(animation);

    }

    private static class FragmentAdapter extends FragmentPagerAdapter {
        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

                return new ReFragment();

        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
