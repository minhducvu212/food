package com.example.minhd.food;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgTw,imgFb ;
    private Button btnLogin ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        fullAct();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewByIds();
    }

    private void findViewByIds() {
        imgTw = (ImageView) findViewById(R.id.btn_tw) ;
        imgFb = (ImageView) findViewById(R.id.btn_fb) ;
        imgFb.setOnClickListener(this);
        imgTw.setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);
    }

    private void fullAct() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_fb:
                Intent intent = new Intent(LoginActivity.this, MainActivity.class) ;
                startActivity(intent);
                break;

            case R.id.btn_tw:
                Intent intent1 = new Intent(LoginActivity.this, MainActivity.class) ;
                startActivity(intent1);
                break;

            case R.id.btn_login:
                Intent intent2 = new Intent(LoginActivity.this, MainActivity.class) ;
                startActivity(intent2);
                break;
        }
    }
}
