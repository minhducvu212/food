package com.example.minhd.food;

/**
 * Created by minhd on 17/07/13.
 */

public class ItemRcl {
    private String name;
    private String author;
    private int imageViewRe ;
    private int imageViewAu ;

    public ItemRcl(String name, String author, int imageViewRe, int imageViewAu) {
        this.name = name;
        this.author = author;
        this.imageViewRe = imageViewRe;
        this.imageViewAu = imageViewAu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getImageViewRe() {
        return imageViewRe;
    }

    public void setImageViewRe(int imageViewRe) {
        this.imageViewRe = imageViewRe;
    }

    public int getImageViewAu() {
        return imageViewAu;
    }

    public void setImageViewAu(int imageViewAu) {
        this.imageViewAu = imageViewAu;
    }
}
