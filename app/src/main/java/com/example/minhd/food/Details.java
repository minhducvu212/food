package com.example.minhd.food;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Details extends AppCompatActivity {

    private LinearLayout mainLayout;

    private int[] images = {R.drawable.recipe1, R.drawable.recipe2, R.drawable.recipe1, R.drawable.recipe2,
            R.drawable.recipe1, R.drawable.recipe2, R.drawable.recipe1, R.drawable.recipe2};

    private View cell;

    private ViewPager viewPager;

    @Override
    public void onBackPressed() {

        if(viewPager != null && viewPager.isShown()){

            viewPager.setVisibility(View.GONE);
        }
        else{

            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.activity_details);

        viewPager = (ViewPager) findViewById(R.id._viewPager);

        mainLayout = (LinearLayout) findViewById(R.id._linearLayout);

        for (int i = 0; i < images.length; i++) {

            cell = getLayoutInflater().inflate(R.layout.cell, null);

            final ImageView imageView = (ImageView) cell.findViewById(R.id._image);
            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    viewPager.setVisibility(View.VISIBLE);
                    viewPager.setAdapter
                            (new Pager(Details.this, images));
                    viewPager.setCurrentItem(v.getId());
                }
            });

            imageView.setId(i);


            imageView.setImageResource(images[i]);

            mainLayout.addView(cell);
        }
    }
}

