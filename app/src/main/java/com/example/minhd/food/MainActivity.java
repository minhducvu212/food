package com.example.minhd.food;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import static com.example.minhd.food.ReFragment.list;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdpterRe.Ire, View.OnClickListener, ViewPager.OnPageChangeListener {

    private RecyclerView rclRe;
    private AdpterRe adpterRe;
    private ViewPager vpRe;
    private TabLayout tabLayout;
    private FragmentAdapter mAdapter;
    private LinearLayout linear;
    private DrawerLayout drawer;
    private ImageView img ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setNavigationBarTintEnabled(true);
        tintManager.setTintColor(Color.parseColor("#20000000"));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        adpterRe = new AdpterRe(this, getBaseContext());
        drawer.openDrawer(GravityCompat.START);
        initviews();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_home) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//        } else if (id == R.id.nav_compose) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//        } else if (id == R.id.nav_recipes) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//
//        } else if (id == R.id.nav_categories) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//        } else if (id == R.id.nav_profile) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//        } else if (id == R.id.nav_setting) {
//            Intent intent = new Intent(MainActivity.this, ViewpagerActivity.class);
//            startActivity(intent);
//        } else if (id == R.id.nav_logout) {
//            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//            startActivity(intent);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ItemRcl getItemRe(int position) {
        return list.get(position);
    }
    private void initviews() {
        vpRe = (ViewPager) findViewById(R.id.vp_Re);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        linear = (LinearLayout) findViewById(R.id.click);
        img = (ImageView) findViewById(R.id.btn_colapse) ;
        linear.setOnClickListener(this);
        mAdapter = new FragmentAdapter(getSupportFragmentManager());
        vpRe.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(vpRe);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#C8996B"));
        tabLayout.setBackgroundResource(R.drawable.shape);
        tabLayout.getTabAt(0).setText("Recpis" + " ");
        tabLayout.getTabAt(1).setText("Wine" + " ");
        tabLayout.getTabAt(2).setText("Profile" + " ");

        vpRe.addOnPageChangeListener(this);

    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private static class FragmentAdapter extends FragmentPagerAdapter {
        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return new ReFragment();

        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
