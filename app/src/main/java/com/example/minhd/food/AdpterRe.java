package com.example.minhd.food;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by minhd on 17/07/13.
 */

public class AdpterRe extends RecyclerView.Adapter<AdpterRe.Re> implements View.OnClickListener {

    private Ire mIn ;
    private Context mContext;

    public AdpterRe(Ire ire, Context context) {
        mIn = ire ;
        mContext = context ;
    }

    @Override
    public Re onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_rcl, parent, false) ;
        return new Re(view);
    }

    @Override
    public void onBindViewHolder(Re holder, int position) {
        ItemRcl itemRcl = mIn.getItemRe(position) ;
        holder.tvName.setText(itemRcl.getName());
        holder.tvAuthor.setText(itemRcl.getAuthor());
        holder.imgRe.setImageResource(itemRcl.getImageViewRe());
        holder.imgAuthor.setImageResource(itemRcl.getImageViewAu());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Details.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mIn.getCount();
    }

    @Override
    public void onClick(View view) {
        startFragmen();
    }

    private void startFragmen() {

    }

    class Re extends RecyclerView.ViewHolder {

        public TextView tvName, tvAuthor ;
        public ImageView imgRe,imgAuthor ;
        public LinearLayout linearLayout ;

        public Re(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.name);
            tvAuthor = itemView.findViewById(R.id.author);
            imgRe = itemView.findViewById(R.id.imgRe);
            imgAuthor = itemView.findViewById(R.id.imgAu) ;
            linearLayout = itemView.findViewById(R.id.layout) ;
        }

    }

    interface Ire {
        int getCount() ;
        ItemRcl getItemRe(int position) ;
    }

}
