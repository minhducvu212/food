package com.example.minhd.food;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class ReFragment extends Fragment implements AdpterRe.Ire {

    private AdpterRe adpterRe ;
    public static List<ItemRcl> list ;
    private RecyclerView rcl;

    public ReFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_re, container, false);

        adpterRe = new AdpterRe(this, getContext()) ;
        rcl = view.findViewById(R.id.rcl_Re) ;
        rcl.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcl.setAdapter(adpterRe);
        rcl.smoothScrollToPosition(0);
        initData();

        return view ;
    }

    public static void initData() {
        list = new ArrayList<>();
        list.add(new ItemRcl("Mandarian Sorbet", "by Jone Doe", R.drawable.img1, R.drawable.user1));
        list.add(new ItemRcl("BlackBerry Sorbet", "by Stanlsik Kirilov", R.drawable.img2, R.drawable.user1));
        list.add(new ItemRcl("Mango Sorbet", "by Jone Doe", R.drawable.img3, R.drawable.user1));
        list.add(new ItemRcl("StrawBerry Sorbet", "by Steve Thomas", R.drawable.img4, R.drawable.user1));
        list.add(new ItemRcl("Apple Sorbet", "by Mark Fragzment", R.drawable.img5, R.drawable.user1));
        list.add(new ItemRcl("Sweet Sorbet", "by Selene Setty", R.drawable.img6, R.drawable.user1));
        list.add(new ItemRcl("Mandarian Sorbet", "by Jone Doe", R.drawable.img1, R.drawable.user1));
        list.add(new ItemRcl("BlackBerry Sorbet", "by Stanlsik Kirilov", R.drawable.img2, R.drawable.user1));
        list.add(new ItemRcl("Mango Sorbet", "by Jone Doe", R.drawable.img3, R.drawable.user1));
        list.add(new ItemRcl("StrawBerry Sorbet", "by Steve Thomas", R.drawable.img4, R.drawable.user1));
        list.add(new ItemRcl("Apple Sorbet", "by Mark Fragzment", R.drawable.img5, R.drawable.user1));
        list.add(new ItemRcl("Sweet Sorbet", "by Selene Setty", R.drawable.img6, R.drawable.user1));
        list.add(new ItemRcl("Mandarian Sorbet", "by Jone Doe", R.drawable.img1, R.drawable.user1));
        list.add(new ItemRcl("BlackBerry Sorbet", "by Stanlsik Kirilov", R.drawable.img2, R.drawable.user1));
        list.add(new ItemRcl("Mango Sorbet", "by Jone Doe", R.drawable.img3, R.drawable.user1));
        list.add(new ItemRcl("StrawBerry Sorbet", "by Steve Thomas", R.drawable.img4, R.drawable.user1));
        list.add(new ItemRcl("Apple Sorbet", "by Mark Fragzment", R.drawable.img5, R.drawable.user1));
        list.add(new ItemRcl("Sweet Sorbet", "by Selene Setty", R.drawable.img6, R.drawable.user1));
        list.add(new ItemRcl("Mandarian Sorbet", "by Jone Doe", R.drawable.img1, R.drawable.user1));
        list.add(new ItemRcl("BlackBerry Sorbet", "by Stanlsik Kirilov", R.drawable.img2, R.drawable.user1));
        list.add(new ItemRcl("Mango Sorbet", "by Jone Doe", R.drawable.img3, R.drawable.user1));
        list.add(new ItemRcl("StrawBerry Sorbet", "by Steve Thomas", R.drawable.img4, R.drawable.user1));
        list.add(new ItemRcl("Apple Sorbet", "by Mark Fragzment", R.drawable.img5, R.drawable.user1));
        list.add(new ItemRcl("Sweet Sorbet", "by Selene Setty", R.drawable.img6, R.drawable.user1));

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ItemRcl getItemRe(int position) {
        return list.get(position);
    }
}
